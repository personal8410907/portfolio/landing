import { Container, Grid, Typography, useMediaQuery } from '@mui/material';
import { CardCompany } from './Common/Card/Company';
import { useTheme } from '../hooks/useTheme';
import { useAppSelector } from '../app/hooks';

const Companies = () => {
  const { theme } = useTheme();
  const { title, description, companies } = useAppSelector((state) => state.company);
  const isMobile = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <Container fixed>
      <Grid container spacing={6} direction="column" justifyContent="center" alignItems="center" pt={5} pb={10}>
        <Grid item xs={12}>
          <Typography fontWeight={600} variant="h3">
            {title}
          </Typography>
        </Grid>
        <Grid item sx={{ width: isMobile ? '100%' : '70%', textAlign: 'center' }}>
          <Typography color={theme.palette.text.secondary} paragraph>
            {description}
          </Typography>
        </Grid>

        <Grid item>
          <Grid container spacing={5} justifyContent="center">
            {companies.map((company, index) => (
              <Grid item key={company.name + index}>
                <CardCompany company={company} />
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Grid>
    </Container>
  );
};

export { Companies };
