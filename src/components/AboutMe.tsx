import { Fragment, useMemo } from 'react';
import { Typography, CssBaseline, Grid, styled, useMediaQuery, Link, Container } from '@mui/material';
import { ITechnology } from '../types/aboutMe';
import { useTheme } from '../hooks/useTheme';
import { useAppSelector } from '../app/hooks';
import imageAboutMe from '../assets/images/about-me.jpeg';

const AboutMeBox = styled('div')(({ theme }) => ({
  margin: theme.spacing(12, 0),

  [theme.breakpoints.up('md')]: {
    backgroundPosition: 'left',
  },
}));

const AboutMeGridLeft = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
}));

const AboutMeGridRight = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 10, 0, 0),

  [theme.breakpoints.down('md')]: {
    padding: theme.spacing(0, 4),
    textAlign: 'center',
  },
}));

const AboutMeImg = styled('img')(({ theme }) => ({
  borderRadius: '50%',
  width: '80%',

  [theme.breakpoints.down('md')]: {
    width: '70%',
  },
}));

const AboutMeBold = styled('b')(({ theme }) => ({
  color: theme.palette.primary.main,
}));

const MasivLink = (
  <Link key="masiv" href="https://masiv.com" target="_blank" rel="noreferrer" underline="none" fontWeight={600}>
    <AboutMeBold> Masiv A Route Mobile Company</AboutMeBold>
  </Link>
);

const BoldTechnologies = (technologies: ITechnology[]) => {
  return technologies.map((technology) => <AboutMeBold key={technology}> {technology},</AboutMeBold>);
};

const AboutMe = () => {
  const { theme } = useTheme();
  const { title, description, technologies } = useAppSelector((state) => state.aboutMe);
  const isMobile = useMediaQuery(theme.breakpoints.down('md'));
  const direction = isMobile ? 'column' : 'row';

  const customDescription = useMemo(() => {
    const descriptionSplatted = description.split(' ');
    const descriptionWithLinks = descriptionSplatted.map((word) => {
      if (word === '{Masiv}') return MasivLink;
      if (word === '{Technologies}') return BoldTechnologies(technologies);
      return ' ' + word;
    });

    return descriptionWithLinks;
  }, [description, technologies]);

  return (
    <Fragment>
      <CssBaseline />
      <AboutMeBox>
        <Container fixed>
          <Grid container spacing={12} direction={direction} justifyContent="center" alignItems="center">
            <Grid item xs={12} md={6}>
              <AboutMeGridLeft>
                <AboutMeImg src={imageAboutMe} alt={imageAboutMe} srcSet={imageAboutMe} />
              </AboutMeGridLeft>
            </Grid>
            <Grid item xs={12} md={6}>
              <AboutMeGridRight>
                <Typography fontWeight={600} variant="h3">
                  {title}
                </Typography>

                <Typography color={theme.palette.text.secondary} paragraph my={3}>
                  {customDescription}
                </Typography>
              </AboutMeGridRight>
            </Grid>
          </Grid>
        </Container>
      </AboutMeBox>
    </Fragment>
  );
};

export { AboutMe };
