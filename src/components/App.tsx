import { ThemeProvider, Divider } from '@mui/material';
import { Provider } from 'react-redux';
import { store } from '../app/store';
import { Welcome } from './Welcome';
import { AboutMe } from './AboutMe';
import { Companies } from './Companies';
import { useTheme } from '../hooks/useTheme';
import { Certifications } from './Certifications';

const App = () => {
  const { theme } = useTheme();

  return (
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <Welcome />
        <Divider variant="middle" />
        <AboutMe />
        <Divider variant="middle" />
        <Companies />
        <Divider variant="middle" />
        <Certifications />
      </Provider>
    </ThemeProvider>
  );
};

export default App;
