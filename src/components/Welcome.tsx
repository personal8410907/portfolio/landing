import { Fragment } from 'react';
import { Typography, CssBaseline, Grid, styled, useMediaQuery, Button, Container } from '@mui/material';
import BrowserUpdatedIcon from '@mui/icons-material/BrowserUpdated';
import waveWelcomeDesktop from '../assets/images/waves/welcome-home-desktop.svg';
import waveWelcomeMobile from '../assets/images/waves/welcome-home-mobile.svg';
import { useTheme } from '../hooks/useTheme';
import { useAppSelector } from '../app/hooks';

const WelcomeBox = styled('div')(({ theme }) => ({
  margin: theme.spacing(10, 0),
  backgroundImage: `url(${waveWelcomeMobile})`,
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'top',
  backgroundSize: 'auto',

  [theme.breakpoints.up('md')]: {
    margin: theme.spacing(10, 0, 0, 0),
    backgroundImage: `url(${waveWelcomeDesktop})`,
    backgroundPosition: 'right',
    backgroundSize: 'contain',
  },
}));

const WelcomeGridLeft = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 0, 0, 10),

  [theme.breakpoints.down('md')]: {
    padding: theme.spacing(0, 4),
    textAlign: 'center',
  },
}));

const WelcomeGridRight = styled('div')(() => ({
  display: 'flex',
  justifyContent: 'end',
  alignItems: 'center',
}));

const WelcomeImg = styled('img')(({ theme }) => ({
  width: 'auto',

  [theme.breakpoints.down('md')]: {
    width: '82%',
  },
}));

const Welcome = () => {
  const { title, subtitle, description, image } = useAppSelector((state) => state.welcome);

  const { theme } = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down('md'));
  const direction = isMobile ? 'column-reverse' : 'row';

  const handleDownloadCV = () => {
    window.open('/CV_Juan_Pablo_CE.pdf', '_blank');
  };

  return (
    <Fragment>
      <CssBaseline />
      <WelcomeBox>
        <Container fixed sx={{ maxWidth: '100% !important', padding: '0px !important' }}>
          <Grid
            container
            rowSpacing={12}
            spacing={12}
            direction={direction}
            justifyContent="center"
            alignItems={isMobile ? 'end' : 'center'}
          >
            <Grid item xs={12} md={6}>
              <WelcomeGridLeft>
                <Typography fontWeight={600} variant="h2">
                  {title}
                </Typography>

                <Typography color={theme.palette.primary.main} fontWeight={600} my={2} variant="h6">
                  {subtitle}
                </Typography>

                <Typography color={theme.palette.text.secondary} paragraph mb={5}>
                  {description}
                </Typography>

                <Button variant="outlined" size="large" endIcon={<BrowserUpdatedIcon />} onClick={handleDownloadCV}>
                  Descargar CV
                </Button>
              </WelcomeGridLeft>
            </Grid>
            <Grid item xs={12} md={6}>
              <WelcomeGridRight>
                <WelcomeImg src={image} alt={image} srcSet={image} />
              </WelcomeGridRight>
            </Grid>
          </Grid>
        </Container>
      </WelcomeBox>
    </Fragment>
  );
};

export { Welcome };
