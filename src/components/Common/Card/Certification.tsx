import { Typography, Button, Card, CardMedia, CardContent, CardActions, Container, Grid } from '@mui/material';
import { CardCertificationProps } from '../../../types/certification';
import { useTheme } from '../../../hooks/useTheme';

const CardCertification = ({ certification }: CardCertificationProps) => {
  const { name, date, logo, link } = certification;
  const { theme } = useTheme();

  const handleDownloadCertificationClick = () => {
    if (!link) return;
    window.open(link, '_blank');
  };

  return (
    <Card
      elevation={3}
      sx={{ width: '250px', height: '350px', borderRadius: theme.shape.borderRadius, margin: 'auto' }}
    >
      <CardMedia component="img" alt={logo} height="200" image={logo} sx={{ objectFit: 'fill' }} />
      <CardContent sx={{ height: '90px' }}>
        <Typography gutterBottom variant="caption" component="div" fontWeight={600} textAlign="center">
          {name}
        </Typography>

        <Typography color={theme.palette.text.secondary} component="div" textAlign="center">
          <small>{date}</small>
        </Typography>
      </CardContent>
      <CardActions>
        <Container fixed>
          <Grid container direction="row" justifyContent="center" alignItems="center" height={10}>
            <Grid item xs={12}>
              <Button
                variant="outlined"
                size="small"
                color="primary"
                sx={{ borderRadius: theme.shape.borderRadius }}
                fullWidth={true}
                onClick={handleDownloadCertificationClick}
              >
                Descargar
              </Button>
            </Grid>
          </Grid>
        </Container>
      </CardActions>
    </Card>
  );
};

export { CardCertification };
