import { useMemo } from 'react';
import { Container, Grid, Typography, styled, Link } from '@mui/material';
import { useTheme } from '../hooks/useTheme';
import Carousel from 'react-material-ui-carousel';
import { CardCertification } from './Common/Card/Certification';
import { useAppSelector } from '../app/hooks';

const CertificationsGridLeft = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 10, 0, 0),

  [theme.breakpoints.down('md')]: {
    padding: theme.spacing(0, 0),
    textAlign: 'center',
  },
}));

const CertificationsBold = styled('b')(({ theme }) => ({
  color: theme.palette.primary.main,
}));

const PlatziLink = (
  <Link key="platzi" href="https://platzi.com" target="_blank" rel="noreferrer" underline="none" fontWeight={600}>
    <CertificationsBold> Platzi</CertificationsBold>
  </Link>
);

const UdemyLink = (
  <Link key="udemy" href="https://udemy.com" target="_blank" rel="noreferrer" underline="none" fontWeight={600}>
    <CertificationsBold> Udemy</CertificationsBold>
  </Link>
);

const Certifications = () => {
  const { theme } = useTheme();
  const { title, description, certifications } = useAppSelector((state) => state.certification);

  const customDescription = useMemo(() => {
    const descriptionSplatted = description.split(' ');
    const descriptionWithLinks = descriptionSplatted.map((word) => {
      if (word === '{Platzi}') return PlatziLink;
      if (word === '{Udemy}') return UdemyLink;
      return ' ' + word;
    });

    return descriptionWithLinks;
  }, [description]);

  return (
    <Container fixed>
      <Grid container justifyItems="center" alignItems="center" py={8} mb={10}>
        <Grid item xs={12} md={6}>
          <CertificationsGridLeft>
            <Typography variant="h3" fontWeight={600}>
              {title}
            </Typography>
            <Typography color={theme.palette.text.secondary} paragraph my={2}>
              {customDescription}
            </Typography>
          </CertificationsGridLeft>
        </Grid>
        <Grid item xs={12} md={6}>
          <Container fixed>
            <Grid container justifyContent="center" alignItems="center" my={5}>
              <Grid item xs={12}>
                <Carousel
                  navButtonsAlwaysVisible
                  stopAutoPlayOnHover
                  cycleNavigation
                  animation="slide"
                  autoPlay
                  navButtonsProps={{
                    style: { backgroundColor: theme.palette.primary.main, color: theme.palette.primary.contrastText },
                  }}
                >
                  {certifications.map((certification, index) => (
                    <CardCertification certification={certification} key={certification.name + index} />
                  ))}
                </Carousel>
              </Grid>
            </Grid>
          </Container>
        </Grid>
      </Grid>
    </Container>
  );
};

export { Certifications };
