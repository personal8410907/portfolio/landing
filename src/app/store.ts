import { configureStore } from '@reduxjs/toolkit';
import WelcomeSlice from '../features/welcomeSlice';
import CompanySlice from '../features/companySlice';
import CertificationSlice from '../features/certificationSlice';
import aboutMeSlice from '../features/aboutMeSlice';

export const store = configureStore({
  reducer: {
    // Add your reducers here
    welcome: WelcomeSlice,
    aboutMe: aboutMeSlice,
    company: CompanySlice,
    certification: CertificationSlice,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
