export type ITechnology = string;

export interface IAboutMeSection {
  title: string;
  description: string;
  technologies: ITechnology[];
}
