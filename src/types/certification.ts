export interface ICertification {
  name: string;
  logo: string;
  link: string;
  date: string;
}

export interface ICertificationSection {
  title: string;
  description: string;
  certifications: ICertification[];
}

export type CardCertificationProps = {
  certification: ICertification;
};
