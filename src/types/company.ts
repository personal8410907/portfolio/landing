import { Technology } from './aboutMe';

export interface ICompany {
  name: string;
  logo: string;
  content: string;
  link: string;
  rol: string;
  date: string;
  technologies: Technology[];
  isActive: boolean;
}

export interface ICompanySection {
  title: string;
  description: string;
  companies: ICompany[];
}

export type CardCompanyProps = {
  company: ICompany;
};
