export interface IWelcome {
  title: string;
  subtitle: string;
  description: string;
  image: string;
}
