import ReactDOM from 'react-dom/client';
import App from './components/App';
import './assets/scss/main.scss';
import './assets/index.css';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(<App />);
