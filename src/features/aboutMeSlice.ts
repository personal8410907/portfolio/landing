import { createSlice } from '@reduxjs/toolkit';
import { IAboutMeSection } from '../types/aboutMe';

const initialState: IAboutMeSection = {
  title: 'Acerca de mí',
  description: `
    Actualmente, trabajo como Frontend Developer en la empresa {Masiv} 
    con la metodología Home Office desde la ciudad de Manizales, Colombia.
    Me gustan los desafíos y aprender cosas nuevas, por eso estoy en constante actualización de mis conocimientos.
    He trabajado con tecnologías como: {Technologies} entre otras.
  `,
  technologies: [
    'JavaScript',
    'TypeScript',
    'Vue.js',
    'Nuxt.js',
    'React',
    'Node.js',
    'Nest.js',
    'MongoDB',
    'PostgreSQL',
    'Docker',
    'Git',
    'Jira',
    'Figma',
    'AWS',
  ],
};

export const AboutMe = createSlice({
  name: 'aboutMe',
  initialState,
  reducers: {},
});

export default AboutMe.reducer;
// export const {} = AboutMe.actions;
