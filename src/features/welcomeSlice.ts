import { createSlice } from '@reduxjs/toolkit';
import imageWelcome from '../assets/images/welcome-home.png';
import { IWelcome } from '../types/welcome';

const initialState: IWelcome = {
  title: '!Hola! Soy Juan Pablo',
  subtitle: 'Frontend Developer | JavaScript | TypeScript | Vue.js | React.js',
  description: `
    Desarrollador Frontend con más de 3 años de experiencia en el desarrollo de aplicaciones web y móviles.
    Me apasiona la tecnología y la programación, siempre estoy aprendiendo nuevas tecnologías y
    herramientas para mejorar mis habilidades y así poder ofrecer un mejor servicio a mis clientes o
    equipo de trabajo.
  `,
  image: imageWelcome,
};

export const WelcomeSlice = createSlice({
  name: 'welcome',
  initialState,
  reducers: {},
});

export default WelcomeSlice.reducer;
// export const {} = WelcomeSlice.actions;
