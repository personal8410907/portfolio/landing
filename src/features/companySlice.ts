import { createSlice } from '@reduxjs/toolkit';
import { ICompanySection } from '../types/company';
import imageNeverbit from '../assets/images/companies/neverbit.png';
import imageMasiv from '../assets/images/companies/masiv.png';
import imageUno27 from '../assets/images/companies/uno27.png';

const initialState: ICompanySection = {
  title: 'Empresas',
  description: `
    He tenido la oportunidad de trabajar en diferentes empresas, en proyectos con baja y alta complejidad, en
    los mismos he tenido retos como: liderar equipos de trabajo, trabajar en equipo, crear aplicaciones desde
    cero y mantener aplicaciones existentes, mejorar el performance y la experiencia del usuario (UX/UI),
    siempre con el objetivo de entregar un producto de calidad.
  `,
  companies: [
    {
      name: 'Masivian SAS',
      logo: imageMasiv,
      content: 'Lorem ipsum',
      link: 'https://masiv.com',
      rol: 'Frontend Developer',
      date: '2021/11 - Present',
      technologies: [
        'HTML',
        'CSS',
        'JavaScript',
        'Vue.js',
        'Vuex',
        'Vue Router',
        'Bulma',
        'Sass',
        'Git',
        'Figma',
        'Azure DevOps',
        'AWS',
      ],
      isActive: true,
    },
    {
      name: 'Uno27 SAS',
      logo: imageUno27,
      content: 'Lorem ipsum',
      link: 'https://uno27.com',
      rol: 'Coordinador TI - Frontend Developer',
      date: '2019/12 - 2021/11',
      technologies: ['HTML', 'CSS', 'JavaScript', 'Vue.js', 'Nuxt.js', 'Sass', 'Git', 'Figma', 'Jira'],
      isActive: true,
    },
    {
      name: 'Neverbit',
      logo: imageNeverbit,
      content: 'Lorem ipsum',
      link: 'https://neverbit.com',
      rol: 'Backend Developer',
      date: '2020/11 - 2021/01',
      technologies: ['PHP', 'Laravel', 'PostgreSQL', 'Git', 'Trello', 'Postman', 'AWS'],
      isActive: true,
    },
  ],
};

export const CompanySlice = createSlice({
  name: 'company',
  initialState,
  reducers: {},
});

export default CompanySlice.reducer;
// export const {} = CompanySlice.actions;
