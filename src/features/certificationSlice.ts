import { createSlice } from '@reduxjs/toolkit';
import logoReactHooks from '../assets/images/certifications/react-hooks.png';
import logoReact from '../assets/images/certifications/react.png';
import logoVite from '../assets/images/certifications/vite.png';
import logoVue3 from '../assets/images/certifications/vue-3.png';
import logoNestJS from '../assets/images/certifications/nestjs.png';
import logoNestJSModular from '../assets/images/certifications/nestjs-modular.png';
import logoNestJSMongoDB from '../assets/images/certifications/nestjs-mongodb.png';
import { ICertificationSection } from '../types/certification';

const initialState: ICertificationSection = {
  title: 'Certificaciones',
  description: `
    Me gusta aprender cosas nuevas y estar en constante actualización
    de mis conocimientos, he tomado algunos cursos en plataformas
    populares como {Platzi} , {Udemy} entre otras, he realizado
    certificaciones en diferentes tecnologías, las cuales me han
    ayudado a mejorar mis habilidades y a tener un mejor entendimiento
    de las necesidades de las empresas o clientes.
  `,
  certifications: [
    {
      name: 'Curso - Profesional de REACT HOOKS',
      logo: logoReactHooks,
      link: '/certifications/certification-react-hooks.pdf',
      date: 'Febrero 2023',
    },
    {
      name: 'Curso - Introducción a REACT',
      logo: logoReact,
      link: '/certifications/certification-react.pdf',
      date: 'Febrero 2023',
    },
    {
      name: 'Curso - Reactividad con Vue.js 3',
      logo: logoVue3,
      link: '/certifications/certification-vue-3.pdf',
      date: 'Febrero 2023',
    },
    {
      name: 'Curso - NestJS Persistencia de Datos con MongoDB',
      logo: logoNestJSMongoDB,
      link: '/certifications/certification-vite.pdf',
      date: 'Febrero 2023',
    },
    {
      name: 'Curso - NestJS Programación Modular',
      logo: logoNestJSModular,
      link: '/certifications/certification-nestjs-modular.pdf',
      date: 'Febrero 2023',
    },
    {
      name: 'Curso - Backend con Nest.js',
      logo: logoNestJS,
      link: '/certifications/certification-nestjs.pdf',
      date: 'Febrero 2023',
    },
    {
      name: 'Curso - Vite.js',
      logo: logoVite,
      link: '/certifications/certification-vite.pdf',
      date: 'Febrero 2023',
    },
  ],
};

export const CertificationSlice = createSlice({
  name: 'certification',
  initialState,
  reducers: {},
});

export default CertificationSlice.reducer;
// export const {} = certificationSlice.actions;
